import Data.Char
-- Ejercicio 1
nextchar' :: Char -> Char
nextchar' a = chr((ord a) +1)

-- Ejercicio 2
fact' :: Integer -> Integer
fact' n = if n == 0
            then 1
            else n * fact' (n-1)

-- Ejercicio 3
take' :: Int -> [a] -> [a]
take' n [x] = if n > 1
                then ([x] !! n) : []
                    take' (n-1) [x]
                else ([x] !! n) : []

--(\x ->length [x]< n)

{-take' n [x] = if n == 1
    then ([x] !! n) : [j]
    else ([x] !! n) : [j]
    -}